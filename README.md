# google-codein-hw

A Node.JS module that prints "Hello World". Made for Google CodeIn.

## How to use:
Run `npm i helloworld-codein`
After, Code Example:

```js
const Module = require('helloworld-codein');

Module.Print();
```
